import React from 'react';
import { Service } from '../../models/service';
import { fetchServices } from '../../api/fetch-service';

export function withServiceData(WrappedComponent: React.ComponentType<any>) {
  class ServiceData extends React.Component<any, { services: Service[] }> {
    static displayName = 'ServiceData';
    constructor(props: any) {
      super(props);
      this.state = {
        services: [],
      };
    }

    componentDidMount(): void {
      fetchServices().then(responseData =>
        this.setState({ services: responseData }),
      );
    }

    render() {
      return <WrappedComponent data={this.state.services} {...this.props} />;
    }
  }
  ServiceData.displayName = `ServiceData(${getDisplayName(WrappedComponent)})`;
  return ServiceData;
}

function getDisplayName(WrappedComponent: React.ComponentType<any>) {
  return WrappedComponent.displayName || WrappedComponent.name || 'Component';
}

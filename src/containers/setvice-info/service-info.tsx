import React, {ReactNode} from 'react';
import { Service } from '../../models/service';
import { withServiceData } from '../service-data/withServiceData';

export class ServiceInfo extends React.Component<{
  children: (props: any) => ReactNode;
  data: Service[];
}> {
  render() {
    return this.props.children(this.props);
  }
}

export const ServiceInfoWithDAta = withServiceData(ServiceInfo);

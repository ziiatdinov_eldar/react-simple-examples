import React from 'react';
import { ThemeContext } from '../../theme-context';
import { withServiceData } from '../service-data/withServiceData';
import { Service } from '../../models/service';
import styles from './service-list.module.scss';
import { ServiceCard } from '../../components/service-card/service-card';

export function ServiceList(props: { data: Service[] }) {
  return (
    <ThemeContext.Consumer>
      {value => (
        <div className={styles['service-list']}>
          {props.data.map(service => (
            <ServiceCard
              id={service.id}
              name={service.name}
              description={service.description}
              price={service.price}
              key={service.id}
              theme={value}
            />
          ))}
        </div>
      )}
    </ThemeContext.Consumer>
  );
}

export const ServiceListWithData = withServiceData(ServiceList);

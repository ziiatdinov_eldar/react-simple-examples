import React from 'react';
import { Service } from '../../models/service';
import { fetchServices } from '../../api/fetch-service';
import styles from './service-table.module.scss';

export class ServiceTable extends React.Component<
  any,
  { services: Service[] }
> {
  constructor(props: any) {
    super(props);
    this.state = {
      services: [],
    };
  }

  componentDidMount(): void {
    fetchServices().then(services => this.setState({ services }));
  }

  render() {
    const { services } = this.state;
    return (
      <table className={styles['service-table']}>
        <tbody>
          {services.map(service => (
            <tr key={service.id} className={styles.row}>
              <td className={styles.col}>{service.name}</td>
              <td className={styles.col}>{service.price}</td>
            </tr>
          ))}
        </tbody>
      </table>
    );
  }
}

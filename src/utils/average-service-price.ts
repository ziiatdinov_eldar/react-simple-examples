import { Service } from '../models/service';

export const averageServicePrice = (service: Service[]) => {
  const sum = service.reduce((acc, next) => {
    return next.price + acc;
  }, 0);
  return service.length === 0 ? 0 : sum / service.length;
};

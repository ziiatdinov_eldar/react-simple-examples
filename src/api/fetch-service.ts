import { Service } from '../models/service';

export function fetchServices(): Promise<Service[]> {
         return Promise.resolve([
           {
             id: 1,
             price: 15,
             description:
               'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid aperiam architecto atque culpa dolores facere incidunt laboriosam mollitia necessitatibus nobis omnis quas quia quidem quo ratione recusandae totam velit, veniam.',
             name: 'Service 1',
           },
           {
             id: 2,
             price: 50,
             description:
               'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab at corporis inventore, ipsa laborum nihil omnis porro soluta! Consequatur delectus excepturi illum in officia omnis placeat quos ullam! Accusamus, officia!',
             name: 'Service 2',
           },
           {
             id: 3,
             price: 300,
             description:
               'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid aperiam architecto atque culpa dolores facere incidunt laboriosam mollitia necessitatibus nobis omnis quas quia quidem quo ratione recusandae totam velit, veniam.',
             name: 'Service 3',
           },
           {
             id: 4,
             price: 632,
             description:
               'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab at corporis inventore, ipsa laborum nihil omnis porro soluta! Consequatur delectus excepturi illum in officia omnis placeat quos ullam! Accusamus, officia!',
             name: 'Service 4',
           },
         ]);
       }

import React from 'react';
import { ButtonProps } from '../../models/button';
import { Theme } from '../../models/theme';
import style from './button.module.scss';

const styles = (theme?: Theme) =>
  `${style.btn} ${
    theme === 'dark' ? `${style['button-dark']}` : `${style['button-light']}`
  }`;

export function Button(props: ButtonProps) {
  return (
    <button className={styles(props.theme)} onClick={props.clickHandler}>
      {props.label}
    </button>
  );
}

import React from 'react';
import { Service } from '../../models/service';
import styles from './service-card.module.scss';
import { Button } from '../button/button';
import { Theme } from '../../models/theme';

const cardStyles = (theme?: Theme) =>
  `${styles['service-card']} ${
    theme === 'dark'
      ? `${styles['service-card_dark']}`
      : `${styles['service-card_light']}`
  }`;

export class ServiceCard extends React.Component<Service, { isOpen: boolean }> {
  constructor(props: Service) {
    super(props);
    this.state = {
      isOpen: false,
    };
  }

  onShow = () => {
    this.setState(state => ({ isOpen: !state.isOpen }));
  };

  render() {
    const { name, description, price } = this.props;
    const { isOpen } = this.state;
    return (
      <div className={cardStyles(this.props.theme)}>
        <div className={styles.header}>
          <span className={styles.title}>{name}</span>
          <Button
            label="Show"
            clickHandler={this.onShow}
            theme={this.props.theme}
          />
        </div>
        {isOpen && (
          <div>
            <p>{description}</p>
          </div>
        )}
        <div className={styles.price}>
          <span>Price: {price}$</span>
        </div>
      </div>
    );
  }
}

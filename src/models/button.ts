import { Theme } from './theme';

export interface ButtonProps {
  label: string;
  clickHandler: () => void;
  theme?: Theme;
}

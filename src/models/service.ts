import {Theme} from "./theme";

export interface Service {
  id: number;
  name: string;
  description: string;
  price: number;
  theme?: Theme;
}

import React from 'react';
import { Theme } from './models/theme';

export const ThemeContext = React.createContext<Theme>('light');
ThemeContext.displayName = 'ThemeContext';

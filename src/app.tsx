import React, { useState } from 'react';
import {
  ServiceList,
  ServiceListWithData,
} from './containers/service-list/service-list';
import styles from './app.module.scss';
import { ThemeContext } from './theme-context';
import { Button } from './components/button/button';
import { Theme } from './models/theme';
import { ServiceCard } from './components/service-card/service-card';
import { ServiceTable } from './containers/setvice-table/setvce-table';
import {
  ServiceInfo,
  ServiceInfoWithDAta,
} from './containers/setvice-info/service-info';
import { Service } from './models/service';
import { averageServicePrice } from './utils/average-service-price';

const data: Service[] = [
  {
    id: 1,
    price: 15,
    description: 'Lorem ipsum dolor sit amet, consectetur adipisicing ',
    name: 'Service 1',
  },
  {
    id: 2,
    price: 50,
    description:
      'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab at corporis inven',
    name: 'Service 2',
  },
];

class App extends React.Component<any, { theme: Theme }> {
  constructor(props: any) {
    super(props);
    this.state = {
      theme: 'light',
    };
  }

  changeTheme = () =>
    this.setState(state => ({
      theme: state.theme === 'light' ? 'dark' : 'light',
    }));

  render() {
    return (
      <>
        <div className={styles.theme}>
          <Button
            clickHandler={this.changeTheme}
            label="Change Theme"
            theme={this.state.theme}
          />
          <span>Current theme: {this.state.theme}</span>
        </div>
        <ThemeContext.Provider value={this.state.theme}>
          <div className={styles.app}>
            <ServiceList data={data} />
          </div>
          <ServiceInfo data={data}>
            {({ data }: { data: Service[] }) => (
                <div>
                  <div>Service count: {data.length}</div>
                  <div>Average price: {averageServicePrice(data)}</div>
                </div>
            )}
          </ServiceInfo>
          <div>
            <ServiceTable />
          </div>
          <div>
            <ServiceInfoWithDAta>
              {({ data }: { data: Service[] }) => (
                <div>
                  <div>Service count: {data.length}</div>
                  <div>Average price: {averageServicePrice(data)}</div>
                </div>
              )}
            </ServiceInfoWithDAta>
          </div>
          <div className={styles.app}>
            <ServiceListWithData />
          </div>
          <div>
            <ServiceInfoWithDAta>
              {({ data }: { data: Service[] }) => (
                <div className={styles.service}>
                  <div>Service count: {data.length}</div>
                </div>
              )}
            </ServiceInfoWithDAta>
          </div>
        </ThemeContext.Provider>
        <ServiceCard
          id={123}
          name={'Test'}
          description={
            'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid aperiam architecto atque culpa dolores facere incidunt laboriosam mollitia necessitatibus nobis omnis quas quia quidem quo ratione recusandae totam velit, veniam.'
          }
          price={345}
          theme={'dark'}
        />
      </>
    );
  }
}

export default App;
